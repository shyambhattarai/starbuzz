package com.hfad.starbuzz;

public class Drink
{
    private String name;
    private String description;
    private int imageResourceId;

    //Array of drinks
    public static Drink[] drinks =
            {
                new Drink("Latte", "A couple of espresso shots with steamed milk", R.drawable.latte),
                new Drink("Cappuccino", "Espresso, hot milk, and a steamed milk foam", R.drawable.cappuccino),
                new Drink("Filter", "Highest quality beans roasted and brewed fresh", R.drawable.filter)
            };

    private Drink (String name, String description, int imageResourceId)
    {
        this.name = name;
        this.description = description;
        this.imageResourceId = imageResourceId;
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getName()
    {
        return this.name;
    }

    public int getImageResourceId()
    {
        return this.imageResourceId;
    }

    public String toString()
    {
        return this.name;
    }

}
