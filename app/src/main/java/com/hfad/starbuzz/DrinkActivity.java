package com.hfad.starbuzz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DrinkActivity extends AppCompatActivity
{
    public static final String EXTRA_DRINKID = "";
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink);

        Intent receivedIntent = getIntent();
        int drinkId = (Integer)receivedIntent.getExtras().get(EXTRA_DRINKID);
        Drink selectedDrink = Drink.drinks[drinkId];

        ImageView drinkPhoto = findViewById(R.id.drinkPhoto);
        TextView drinkName = findViewById(R.id.drinkName);
        TextView drinkDesc = findViewById(R.id.drinkDescription);

        //Filling the views with drink details
        drinkName.setText(selectedDrink.getName());
        drinkDesc.setText(selectedDrink.getDescription());
        drinkPhoto.setImageResource(selectedDrink.getImageResourceId());
        drinkPhoto.setContentDescription(selectedDrink.getName());
    }
}
